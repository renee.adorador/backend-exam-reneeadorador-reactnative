//FetchPosts.js


const URI = 'http://localhost:8000'; //url from laravel

export default{
	//export an async method
	async fetchPosts(){
		try{
			let response = await fetch(URI+'/api/posts');//response from URI
			let responseJsonData = await response.json();//the json response after request
			return responseJsonData; //return data
		}catch(e){
			console.log(e);
		}
	}
}